#!/bin/bash

#Make errors stop execution and add debugging
set -ex

echo "Running script test.sh"

echo "Calling gradle wrapper to test"
./gradlew test
