# Do NOT use. It's a Proof Of Concept repo for Spring Dependency Injection


## java-dateutil-api
Defines the API for the DateUtil library, such as getting the current date, etc.

## How to use in your project
1. Add the following dependency to gradle build (set the context as required). Please check latest version in build.gradle (this README will not be updated every time the version number is increased).

         compile ('com.workforcesoftware:java-dateutil-api:0.1.0')

2. After doing this, the DateUtilService interface will be available for use in your project.

## How to build/test (for doing additional development of the library)
Clone repository and then run:
    
    ./gradlew build

You can also run the following to execute just the test cases (they are executed during gradlew build):

    ./gradlew test

To re-run tests if all tasks are up-to-date run:
    
    ./gradlew cleanTest test

## Contributions are welcome
Changes and contributions to this project are encouraged, and can be made by raising a pull request.

We suggest you read through the [Coding Standards](http://standards.tools.workforcefs.com) first.

All pull requests must follow the
[Lighthouse Code Review](http://standards.tools.workforcefs.com/00-source_control/01-lighthouse_code_reviews.html) process.

