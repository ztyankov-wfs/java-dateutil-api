#!/bin/bash

#Make errors stop execution and add debugging
set -ex

echo "Running script build.sh"

#Do not run the tests.  We have a separate stage in Jenkinsfile for executing the tests.
./gradlew build -x test
