package com.workforcesoftware.utilitylibrary.util;

//------------------------------------------------------------------
// Copyright (c) 1999, 2019
// WorkForce Software, Inc.
// All rights reserved.
//
// Web-site: http://www.workforcesoftware.com
// E-mail:   support@workforcesoftware.com
// Phone:    (877) 493-6723
//
// This program is protected by copyright laws and is considered
// a trade secret of WorkForce Software.  Access to this program
// and source code is granted only to licensed customers.  Under
// no circumstances may this software or source code be distributed
// without the prior written consent of WorkForce Software.
// -----------------------------------------------------------------

import com.workforcesoftware.dateutil.config.Config;
import com.workforcesoftware.dateutil.util.DateUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan("com.workforcesoftware.utilitylibrary")
public class DateUtilTest {

  @Test
  public void testCreateDateFromString() {
    String dateAsString = "2019-11-15";
    LocalDate date = DateUtil.getDateFromString(dateAsString);

    Assert.assertEquals(dateAsString, date.toString());
  }

  @SpringBootApplication
  @Import({Config.class})
  static class TestConfiguration {

  }
}
