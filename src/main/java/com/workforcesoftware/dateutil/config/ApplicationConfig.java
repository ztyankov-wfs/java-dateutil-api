package com.workforcesoftware.dateutil.config;
//------------------------------------------------------------------
// Copyright (c) 1999, 2020
// WorkForce Software, Inc.
// All rights reserved.
//
// Web-site: http://www.workforcesoftware.com
// E-mail:   support@workforcesoftware.com
// Phone:    (877) 493-6723
//
// This program is protected by copyright laws and is considered
// a trade secret of WorkForce Software.  Access to this program
// and source code is granted only to licensed customers.  Under
// no circumstances may this software or source code be distributed
// without the prior written consent of WorkForce Software.
// -----------------------------------------------------------------

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;

/**
 * This class dynamically loads all properties defined in application.properties.
 * Make it available for application to use it anywhere in the code as it is a singleton class.
 */
public class ApplicationConfig {

  private ApplicationConfig() {}

  /**
   * Returns the global application configuration, creating one if this is the first time the application configuration object is being used.
   * Uses the singleton pattern to enforce single instance creation.
   * Uses double check locking mechanism to avoid synchronization on every time accessing the object; thus improve performance.
   */
  public static ApplicationConfig getInstance() {
    ApplicationConfig appConfigProp = applicationConfig;
    if (appConfigProp == null) {
      synchronized (ApplicationConfig.class) {
        appConfigProp = applicationConfig;
        if (appConfigProp == null) {
          appConfigProp = new ApplicationConfig();
          applicationConfig = appConfigProp;
          appConfigProp.initializePropertiesMap();
        }
      }
    }
    return appConfigProp;
  }

  /**
   * Return value of specified property
   *
   * @param propertyName property to retrieve
   * @return the value to which this configuration maps the specified key, or
   * null if the configuration contains no mapping for this key.
   */
  public String getPropertyValue(String propertyName) {
    return (String) config.getProperty(propertyName);
  }

  private void initializePropertiesMap() {
    Parameters params = new Parameters();
    // Read application properties from file
    File propertiesFile = new File(APP_CONFIG_FILE_PATH);
    FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
        new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
            .configure(params.fileBased()
                .setFile(propertiesFile));

    try {
      // load all properties read from the file
      config = builder.getConfiguration();
    } catch (ConfigurationException cex) {
      LOGGER.error("failed to load properties from file " + APP_CONFIG_FILE_PATH, cex);
      throw new RuntimeException("application configuration initialization failed");
    }
  }

  /**
   * The global application configuration object(singleton).
   */
  private static ApplicationConfig applicationConfig;

  /**
   * It holds application configuration properties and their values
   */
  private Configuration config = null;

  /**
   * Logger used for this class.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfig.class);

  /**
   * File path for application.properties file
   */
  private static final String APP_CONFIG_FILE_PATH = "config/application.properties";

}
