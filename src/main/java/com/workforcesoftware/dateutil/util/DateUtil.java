package com.workforcesoftware.dateutil.util;
//------------------------------------------------------------------
// Copyright (c) 1999, 2020
// WorkForce Software, Inc.
// All rights reserved.
//
// Web-site: http://www.workforcesoftware.com
// E-mail:   support@workforcesoftware.com
// Phone:    (877) 493-6723
//
// This program is protected by copyright laws and is considered
// a trade secret of WorkForce Software.  Access to this program
// and source code is granted only to licensed customers.  Under
// no circumstances may this software or source code be distributed
// without the prior written consent of WorkForce Software.
// -----------------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Utility class that provides utility methods to work with dates
 */
public class DateUtil {
  /**
   * gets the Date object from string.
   * @param date string date.
   * @return Date or null if date string in not valid date format.
   */
  public static LocalDate getDateFromString(String date) {
    LocalDate parseDate = null;
    try {
      final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(DATE_FORMAT);
      parseDate = LocalDate.parse(date, dateFormat);
    } catch (DateTimeParseException e) {
      LOGGER.error("Error parsing date: ", e);
    }
    return parseDate;

  }

  /** Logger used for this class. */
  private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

  /** Format for dates used by this service. */
  private static final String DATE_FORMAT = "yyyy-MM-dd";
}
